# must be run on Ubuntu 20 VM

# Install docker
sudo apt update &&
sudo apt install apt-transport-https ca-certificates curl software-properties-common -y && 
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add - && 
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu focal stable" &&

sudo apt update && 
sudo apt install docker-ce -y &&

sudo systemctl start docker && 
sudo systemctl enable docker && 

sudo usermod -aG docker ${USER} && 
newgrp docker &&

### INSTALL K3S
curl -sfL https://get.k3s.io | sh -s - --docker --no-deploy=traefik &&
sudo kubectl get nodes -o wide &&

### INSTALL helmv3
wget https://get.helm.sh/helm-v3.4.2-linux-amd64.tar.gz && 
tar -xvzf helm-v3.4.2-linux-amd64.tar.gz &&
sudo cp linux-amd64/helm /usr/local/bin/. &&
helm repo add stable https://charts.helm.sh/stable && 
helm repo update
