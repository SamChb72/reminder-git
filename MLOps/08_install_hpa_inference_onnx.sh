# horizontal scale
kubectl -n platform-mlops autoscale deployment inference-engine-onnx --min=1 --max=10 --cpu-percent=65
