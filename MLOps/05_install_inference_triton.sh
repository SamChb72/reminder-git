#!/bin/bash

# Needs bucket-1/models/ directory to be there to avoid server crashloop
kubectl -n platform-mlops apply -f inference-engine-triton.yml

echo "Installing... inference engine (can be very long download more than 30 min)"
kubectl -n platform-mlops rollout status deployment inference-engine-triton
