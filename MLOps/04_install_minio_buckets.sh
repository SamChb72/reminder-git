#!/bin/bash

MINIO_ACCESSKEY="ACCESS_KEY"
MINIO_SECRETKEY="SECRET_KEY"
MINIO_IP="phi.greenflops.com"

# Create empty model directory
echo "create bucket-1 template $MINIO_IP:9000"
cd python_scripts && docker build -t minio-init-bucket:latest . &&  docker run -it minio-init-bucket:latest $MINIO_IP:9000 $MINIO_ACCESSKEY $MINIO_SECRETKEY
cd ..

