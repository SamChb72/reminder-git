## Knative Serving
kubectl apply --filename https://github.com/knative/serving/releases/download/v0.19.0/serving-crds.yaml
kubectl apply --filename https://github.com/knative/serving/releases/download/v0.19.0/serving-core.yaml


## Istio
## install istioctl
wget https://github.com/istio/istio/releases/download/1.7.6/istioctl-1.7.6-osx.tar.gz
tar -xvzf istioctl-1.7.6-osx.tar.gz 

cat << EOF > ./istio-minimal-operator.yaml
apiVersion: install.istio.io/v1alpha1
kind: IstioOperator
spec:
  values:
    global:
      proxy:
        autoInject: disabled
      useMCP: false
      # The third-party-jwt is not enabled on all k8s.
      # See: https://istio.io/docs/ops/best-practices/security/#configure-third-party-service-account-tokens
      jwtPolicy: first-party-jwt

  addonComponents:
    pilot:
      enabled: true

  components:
    ingressGateways:
      - name: istio-ingressgateway
        enabled: true
EOF

./istioctl install -f istio-minimal-operator.yaml

## Knative Istio controler
kubectl apply --filename https://github.com/knative/net-istio/releases/download/v0.19.0/release.yaml

## Auto scaling
kubectl apply --filename https://github.com/knative/serving/releases/download/v0.19.0/serving-hpa.yaml

## Disable zero scaling
kubectl apply -f knative-zeroscaling.yaml

## Tag header for revision routing
kubectl patch cm config-features -n knative-serving -p '{"data":{"tag-header-based-routing":"Enabled"}}'


## Lets'Encrypt
#kubectl apply --filename https://github.com/knative/net-http01/releases/download/v0.19.0/release.yaml

######
## Install kn
wget https://storage.googleapis.com/knative-nightly/client/latest/kn-darwin-amd64
chmod a+x kn-darwin-amd64

./kn-darwin-amd64 -n platform-mlops service create greeter --image=quay.io/rhdevelopers/knative-tutorial-greeter:quarkus
./kn-darwin-amd64 -n platform-mlops service describe greeter

