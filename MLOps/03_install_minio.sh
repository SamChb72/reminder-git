#!/bin/bash

MINIO_ACCESSKEY="ACCESS_KAY"
MINIO_SECRETKEY="SECRET_KEY"
#externalIPs="192.168.122.110"
#service.externalIPs=$externalIPs

# Storage System minio

helm install local-storage --namespace platform-mlops --set accessKey=$MINIO_ACCESSKEY,secretKey=$MINIO_SECRETKEY,service.type=LoadBalancer,persistence.storageClass=local-path stable/minio

#secure
#helm install local-storage --namespace platform-mlops --set tls.enabled,,accessKey=$MINIO_ACCESSKEY,secretKey=$MINIO_SECRETKEY,service.type=LoadBalancer,persistence.storageClass=local-path stable/minio

echo "Installing... minio server"
kubectl -n platform-mlops rollout status deployment local-storage-minio
