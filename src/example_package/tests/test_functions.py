from unittest import TestCase

from example_package import functions

class TestPKG(TestCase):
    def test_answer(self):
        assert functions.add_one(3) == 4
