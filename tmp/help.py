import argparse

parser=argparse.ArgumentParser(
    description='''Display next trains. ''',
    epilog="""""")
parser.add_argument('--type', type=int, default=42, help='TYPE The type of transport (metros, rers, tramways, buses or noctiliens)')
parser.add_argument('--line', type=int, default=42, help='LINE The code of transport (eg. 8)')
parser.add_argument('--station', type=int, default=42, help='STATION The station name (e.g. bonne nouvelle)')
parser.add_argument('--way', type=int, default=43, help='WAY Way on the line')
args=parser.parse_args()

print("start the program")
