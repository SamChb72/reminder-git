import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name="example-pkg-GREENFLOWS",
    version="0.0.7",
    author="Samy CHBINOU",
    author_email="samy.chbinou@greenflows.fr",
    description="A small example package",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/SamChb72/reminder-git",
    project_urls={
        "Bug Tracker": "https://gitlab.com/SamChb72/reminder-git/-/issues",
    },
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    package_dir={"": "src"},
    packages=setuptools.find_packages(where="src"),
    python_requires=">=3.6",
    test_suite='nose.collector',
    tests_require=['nose'],
)
