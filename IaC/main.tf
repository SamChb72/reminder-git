terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.27"
    }
  }

  required_version = ">= 0.14.9"
}

provider "aws" {
  profile = "default"
  region  = "eu-west-3"
}

resource "aws_instance" "sandbox_gitlabrunner" {
  ami           = "ami-CI"
  instance_type = "t2.micro"
  key_name = "aws-ssh-key"
  tags = {
    Name = "sandbox_gitlabrunner"
  }
  security_groups = ["${aws_default_security_group.default.name}"]
}

resource "aws_key_pair" "aws-ssh-key" {
  key_name   = "aws-ssh-key"
  public_key = "ssh-rsa XXXXXXXX samy@sphere"
}

resource "aws_default_security_group" "default" {
   vpc_id      = "${aws_default_vpc.default.id}"
 ingress {
     # TLS (change to whatever ports you need)
     from_port   = 22
     to_port     = 22
     protocol    = "tcp"
     # Please restrict your ingress to only necessary IPs and ports.
     # Opening to 0.0.0.0/0 can lead to security vulnerabilities.
     cidr_blocks     = ["IP.IP.IP.IP/32"]
   }
 egress {
     from_port       = 0
     to_port         = 0
     protocol        = "-1"
     cidr_blocks     = ["0.0.0.0/0"]
   }
 }

resource "aws_default_vpc" "default" {
   tags = {
     Name = "Default VPC"
   }
 }
