#!/bin/bash

remote_host=$1
remote_user=greenflows
local_script=gitlabrunner-install.sh

# Copy local script to remote host
scp -o StrictHostKeyChecking=no -i /kvm/iso/keys/id_rsa ${local_script} ${remote_user}@${remote_host}:/tmp/.

# run script on remote host
ssh -i /kvm/iso/keys/id_rsa -t ${remote_user}@${remote_host} "sudo /tmp/${local_script}"
