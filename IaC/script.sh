#!/bin/bash

#mkdir ssh-keys && cd ssh-keys && ssh-keygen -t rsa -f id_rsa_aws && cd ..
terraform init
terraform plan -out=terraform.plan
terraform apply terraform.plan
#terraform destroy

sleep 20

address=`grep public_dns terraform.tfstate | awk '{ print $2 }' | sed s/[\",]//g`
echo "connecting to ${address}"
ssh -o StrictHostKeyChecking=no -i ssh-keys/id_rsa_aws ubuntu@${address}
