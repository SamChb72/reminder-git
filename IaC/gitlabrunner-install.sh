#!/bin/bash

USER=greenflows
RUNNER_TAG=runner-shell
URL=https://gitlab.com
TOKEN=7Hm-GEu3CCTWi-N7BHwo
EXECUTOR=shell

# update ubuntu
sudo apt update && sudo upgrade -y
sudo apt install -y python3-pip
sudo ln -s /usr/bin/python3 /usr/bin/python
sudo ln -s /usr/bin/pip3 /usr/bin/pip

# install gitlab runner
sudo curl -L --output /usr/local/bin/gitlab-runner "https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64"
sudo chmod +x /usr/local/bin/gitlab-runner
sudo gitlab-runner install --user=$USER --working-directory=/home/$USER
sudo gitlab-runner start
sleep 30
#sudo systemctl status gitlab-runner.service

# register
sudo gitlab-runner register --non-interactive -name ${HOSTNAME} --url ${URL} --registration-token ${TOKEN} --executor ${EXECUTOR} --tag-list "${RUNNER_TAG}" --locked="true"
sudo gitlab-runner restart

# netdata
#bash <(curl -Ss https://my-netdata.io/kickstart.sh) --dont-wait

echo DONE !!!

