import sys
from minio import Minio
#from minio.error import ResponseError

url=sys.argv[1]
access_key=sys.argv[2]
secret_key=sys.argv[3]

minioClient = Minio(
    url,
    access_key,
    secret_key,
    secure=False
)

minioClient.make_bucket('bucket-1')
minioClient.fput_object('bucket-1', 'models/README', '/app/README')
minioClient.fput_object('bucket-1', 'datasets-test/README', '/app/README')
minioClient.fput_object('bucket-1', 'datasets-training/README', '/app/README')
minioClient.fput_object('bucket-1', 'qa-metrics/README', '/app/README')
